package com.iitu.critique.controller;

import com.iitu.critique.model.Critique;
import com.iitu.critique.service.CritiqueService;
import com.iitu.critique.service.Producer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/critiques")
@Api(value = "Critique controller", description = "Controller allows to get critiques of user")
public class CritiqueController {

    private final Producer producer;
    @Autowired
    private CritiqueService critiqueService;

    public CritiqueController(Producer producer) {
        this.producer = producer;
    }

    @ApiOperation(value = "To get all critiques from the database", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("")
    public ResponseEntity<?> getAllCritiques() {
        return ResponseEntity.ok(critiqueService.getAllCritiques());
    }


    @ApiOperation(value = "To get all critiques of recipe", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/recipe/{id}")
    public ResponseEntity<?> getAllCritiquesOfUser(@PathVariable Long id){
        return ResponseEntity.ok(critiqueService.getUserCritiques(id));
    }

    @ApiOperation(value = "To get specific critique by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/{id}")
    public ResponseEntity<?> getCritiqueById(@PathVariable Long id){
        return ResponseEntity.ok(critiqueService.getCritiqueById(id));
    }


    @ApiOperation(value = "To create critique", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PostMapping("")
    public ResponseEntity<?> addCritique(@RequestBody Critique critique) {
        critiqueService.addCritique(critique);
        this.producer.critiqueRequestNotify(critique);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "To delete specific critique by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCritique(@PathVariable Long id) {
        critiqueService.deleteCritique(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @ApiOperation(value = "To update critique by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PutMapping("/{id}")
    public ResponseEntity<?> updateCritique(@PathVariable Long id, @RequestBody Critique critique) {
        critique.setId(id);
        critiqueService.updateCritique(critique);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
