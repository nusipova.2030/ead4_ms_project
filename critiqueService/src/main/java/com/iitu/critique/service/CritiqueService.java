package com.iitu.critique.service;

import com.iitu.critique.model.Critique;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CritiqueService {
    List<Critique> getAllCritiques();
    List<Critique> getUserCritiques(Long recipeId);
    List<Critique> getAuthorComments(Long authorId);
    Critique getCritiqueById(Long id);

    void addCritique(Critique critique);
    void deleteCritique(Long id);
    void updateCritique(Critique critique);
}
