package kz.iitu.mealService.controller;

import kz.iitu.mealService.model.Meal;
import kz.iitu.mealService.service.MealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;

@RestController
@RequestMapping("/meals")
@Api(value = "Meal controller", description = "Controller allows to get meals")
public class MealController {
    @Autowired
    private MealService mealService;

    @ApiOperation(value = "To get all meals from the database", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("")
    public ResponseEntity<?> getAllMeals(){
        return ResponseEntity.ok(mealService.findAllMeals());
    }

    @ApiOperation(value = "To get specific category related meals", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/category/{id}")
    public ResponseEntity<?> getCategoryProducts(@PathVariable Long id){
        return ResponseEntity.ok(mealService.getCategoryMeals(id));
    }

    @ApiOperation(value = "To get meal by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/{id}")
    public ResponseEntity<?> findMealById(@PathVariable Long id) {
        return ResponseEntity.ok(mealService.getMealById(id));
    }

    @ApiOperation(value = "To get recipes related to specific meal by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @GetMapping("/{id}/recipes")
    public ResponseEntity<?> getAllRecipesOfMeal(@PathVariable Long id) {
        return ResponseEntity.ok(mealService.getAllRecipesOfMeal(id));
    }

    @ApiOperation(value = "To create new meal", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PostMapping("")
    public ResponseEntity<?> createProduct(@RequestBody Meal meal){
        mealService.addMeal(meal);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }

    @ApiOperation(value = "To remove meal by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable Long id){
        mealService.deleteMeal(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @ApiOperation(value = "To update meal by id", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successful"),
            @ApiResponse(code = 401, message = "Not authorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not found")
    }
    )
    @PutMapping("/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable Long id,@RequestBody Meal meal){
        meal.setId(id);
        mealService.updateMeal(meal);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}
