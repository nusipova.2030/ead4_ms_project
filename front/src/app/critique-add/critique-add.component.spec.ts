import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CritiqueAddComponent } from './critique-add.component';

describe('CritiqueAddComponent', () => {
  let component: CritiqueAddComponent;
  let fixture: ComponentFixture<CritiqueAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CritiqueAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CritiqueAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
