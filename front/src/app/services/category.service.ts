import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {GlobalVariable} from "../global";
import { Observable } from "rxjs";


@Injectable({
  providedIn: 'root',
})
export class CategoryService {

  constructor(private http: HttpClient){

  }
  getCat(): Observable<any> {
    return this.http
      .get<any>(`${GlobalVariable.BASE_API}/categories`);
  }
  getMeals(id: any): Observable<any> {
    return this.http
      .get<any>(`${GlobalVariable.BASE_API}/categories/${id}/meals`);
  }
  getAllMeals(): Observable<any> {
    return this.http
      .get<any>(`${GlobalVariable.BASE_API}/meals`);
  }

  getRecipesofMeal(id: any): Observable<any> {
    return this.http
      .get<any>(`${GlobalVariable.BASE_API}/meals/${id}/recipes`);
  }
  getMeal(id: number): Observable<any> {
    return this.http
      .get<any>(`${GlobalVariable.BASE_API}/meals/${id}`);
  }
}
