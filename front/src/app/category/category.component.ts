import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categories = [];
  constructor(private primengConfig: PrimeNGConfig,
    private catService: CategoryService) {}

  ngOnInit() {
      this.primengConfig.ripple = true;

    this.catService.getCat().subscribe(res => {
      this.categories = res
    })

  }

}
