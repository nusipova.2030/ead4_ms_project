import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { MessageService } from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  username!: string;
  password!: string;
  user: any = null;
  userName: any = null;
  constructor(private auth: AuthService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.user = this.auth.currentUserValue;
    this.userName = JSON.parse(localStorage.getItem("currentUserName") || '{}');
    console.log(localStorage.getItem('currentUserName'), this.userName['username']);

    console.log(this.auth.currentUserValue);
  }
  login(){
    let user = {
      "username": this.username,
      "password": this.password
    }
    this.auth.login(user).subscribe(
      (res) => {
        // console.log(res);
        // if (res['accessToken']){
        //   this.ngOnInit();
        // }
        // if(res['error']){
        //   console.log('error');
          this.messageService.add({severity:'success', summary: 'Success', detail: 'Successfully logged in!'});
      },
      (error: any) => {
        console.log('error');
        // alert("Oops");
        this.messageService.add({
          severity: 'error',
          summary: 'Error',
          detail: error.error.error ,
        });
      },
      () => {
        this.ngOnInit();
      }
    );

  }
  logout(){
    this.auth.logout();
  }
  add(){
    this.router.navigate(['recipe/add']);
  }

}
