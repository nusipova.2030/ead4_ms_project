import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoryComponent } from './category/category.component';
import { MainComponent } from './main/main.component';
import { MealComponent } from './meal/meal.component';
import { RecipeAddComponent } from './recipe-add/recipe-add.component';
import { RecipePageComponent } from './recipe-page/recipe-page.component';
import { RecipeComponent } from './recipe/recipe.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'categories',
        component: CategoryComponent,
      },
      {
        path: '',
        component: CategoryComponent,
      },
      { path: 'categories/:id' ,
        component: MealComponent
      },
      { path: 'meals/:id' ,
        component: RecipeComponent
      },
      { path: 'recipe/add' ,
        component: RecipeAddComponent
      },
      { path: 'recipe/:id' ,
        component: RecipePageComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
