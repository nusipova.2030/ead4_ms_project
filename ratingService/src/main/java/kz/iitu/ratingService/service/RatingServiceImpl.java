package kz.iitu.ratingService.service;

import kz.iitu.ratingService.model.Rating;
import kz.iitu.ratingService.repository.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RatingServiceImpl implements RatingService {
    @Autowired
    RatingRepository repository;

    @Override
    public List<Rating> getAllRatings() {
        return repository.findAll();
    }

    @Override
    public double getRecipeRating(Long recipeId) {
        List<Rating> ratingList = repository.findByRecipeId(recipeId);
        double sum = 0;
         for (Rating r : ratingList){
             sum += Integer.parseInt(r.getRating());
         }
        return sum/ratingList.size();
    }

    @Override
    public void addRating(Rating rating) {
        repository.save(rating);
    }

    @Override
    public void deleteRating(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateRating(Rating rating) {
        Optional<Rating> ratingOptional = repository.findById(rating.getId());

        if (ratingOptional.isPresent()) {
            Rating r = ratingOptional.get();
            r.setRating(rating.getRating());
            repository.saveAndFlush(r);
        }
    }
}
