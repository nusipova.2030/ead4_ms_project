package kz.iitu.ratingService.service;

import kz.iitu.ratingService.model.Rating;

import java.util.List;

public interface RatingService {
    List<Rating> getAllRatings();
    double getRecipeRating(Long recipeId);

    void addRating(Rating rating);
    void deleteRating(Long id);
    void updateRating(Rating rating);
}
