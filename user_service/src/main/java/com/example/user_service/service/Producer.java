package com.example.user_service.service;

import com.example.user_service.model.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {
    private static final String TOPIC = "recipe_requests";

    @Autowired
    private KafkaTemplate<String, Recipe> kafkaTemplate;

    public String bookRequestNotify(Recipe recipe) {
        System.out.println(String.format("#### -> Producing recipe request to notification service -> %s", recipe));
        this.kafkaTemplate.send(TOPIC, recipe);
        return "Successfully";
    }
}
