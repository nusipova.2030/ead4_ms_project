package com.example.user_service.service;

import com.example.user_service.model.Quicklinks;
import com.example.user_service.model.Recipe;
import com.example.user_service.model.User;
import com.example.user_service.repository.RoleRepository;
import com.example.user_service.repository.UserRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findById(id).get();
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "findAllUserRecipesFallback",
            threadPoolKey = "findAllUserRecipes",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
            })
    public List<Recipe> findAllUserRecipes(Long id) {
        List<Recipe> recipeList  = restTemplate.getForObject("http://recipe-service/recipes/author/"+ id, List.class);
        return recipeList;
    }



    public List<Recipe>  findAllUserRecipesFallback(Long id) {
        List<Recipe> recipeList = new ArrayList<>();
        Recipe r = new Recipe();
        r.setId(0L);
        r.setName("Name is not available: Service Unavailable");
        recipeList.add(r);
        return recipeList;
    }


    @Override
    @HystrixCommand(
            fallbackMethod = "findAllUserQuicklinksFallback",
            threadPoolKey = "findAllUserQuicklinks",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
            })
    public List<Recipe> findAllUserQuicklinks(Long id) {
        List<Quicklinks> list = restTemplate.getForObject("http://quicklinks-service/user/" + id, List.class);
        System.out.println("qq "+ id+ list);
        List<Recipe> rList = new ArrayList<>();
        for (Quicklinks l: list){
            Recipe r = restTemplate.getForObject("http://recipe-service/"+ id, Recipe.class);
            rList.add(r);
        }
        return rList;
    }
    public List<Recipe> findAllUserQuicklinksFallback(Long id) {
        List<Recipe> rList = new ArrayList<>();
        rList.add(new Recipe(1L,"Recipe is not available: Service Unavailable"," ",1L,1L));
        return rList;
    }

    @Override
    public void createUser(User user) {
        userRepository.save(user);
    }

    @Override
    public void createRecipe(Recipe recipe) {

    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public void updateUser(User user) {
        Optional<User> userOptional = userRepository.findById(user.getId());

        if (userOptional.isPresent()) {
            User dbUser = userOptional.get();
            dbUser.setUsername(user.getUsername());
            dbUser.setPassword(user.getPassword());
            userRepository.saveAndFlush(dbUser);
        }
    }


//    private BCryptPasswordEncoder passwordEncoder;


}
