package kz.iitu.zuul_server.common;

public enum UserRole {
    USER, ADMIN;
}
